package com.example.app03calculoareaperimetro;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Rectangulo_Activity extends AppCompatActivity {

    //Atributo
    Rectangulo rectangulo;
    private EditText txtBase;
    private EditText txtAltura;
    private TextView txtCalArea;
    private TextView txtCalPerimetro;
    private TextView txtNombree;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private void iniciar(){
        txtNombree =  findViewById(R.id.etxtNombre);
        txtBase=findViewById(R.id.etxtBase);
        txtAltura = findViewById(R.id.etxtAltura);
        txtCalArea = findViewById(R.id.etxtArea);
        txtCalPerimetro = findViewById(R.id.etxtPerimetro);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

    }
    private boolean validar(){
        boolean exito = true;
        if(txtAltura.getText().toString().equals("")) exito = false;
        if(txtBase.getText().toString().equals("")) exito = false;
        return exito;
    }
    private void btnCalcular(){
        rectangulo = new Rectangulo(
                Integer.parseInt(txtBase.getText().toString()),
                Integer.parseInt(txtAltura.getText().toString())
        );
        float area = rectangulo.calcularCalcularArea();
        float perimetro = rectangulo.calcularPagPerimetro();
        txtCalArea.setText(" " + area);
        txtCalPerimetro.setText(" " + perimetro);
    }
    private void btnLimpiar(){
        txtAltura.setText("");
        txtBase.setText("");
        txtCalArea.setText("");
        txtCalPerimetro.setText("");

        rectangulo = new Rectangulo();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);
        iniciar();
        String nombre = getIntent().getStringExtra("nombre");
        txtNombree.setText(nombre);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Rectangulo_Activity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(validar())
                {
                    btnCalcular();
                }
                else{
                    Toast.makeText(getApplicationContext(),"Falto capturar datos", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLimpiar();
            }
        });


    }
}