package com.example.app03calculoareaperimetro;

import androidx.appcompat.app.AppCompatActivity;

public class Rectangulo extends AppCompatActivity {

    private int base;
    private int altura;

    public Rectangulo(){
        this.base=0;
        this.altura=0;
    }

    public Rectangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    public Rectangulo(Rectangulo rectangulo){
        this.base = rectangulo.base;
        this.altura = rectangulo.altura;
    }

    public int getBase() { return base;}
    public void setBase(int base) { this.base = base;}

    public int getAltura() { return altura;}

    public void setAltura(int altura) { this.altura = altura;}

    public float calcularCalcularArea(){
        float area = this.altura;
        area= area * base;
        return area;
    }
    public float calcularPagPerimetro(){
        float perimetro = this.base;
        perimetro = 2 * (base + altura );
        return perimetro;
    }
}
